import { Routes } from '@angular/router'
import { AuthGuard } from './core/guards/auth.guard';

export const appRoutes: Routes = [
    { path: '', redirectTo: 'welcome', pathMatch: 'full' },
    { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomeModule' },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    { path: 'main', loadChildren: './main/main.module#MainModule', canActivate: [AuthGuard] }
]