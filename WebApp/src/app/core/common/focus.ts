// import { Directive, Input, ElementRef } from '@angular/core';

// @Directive({
// 	selector : '[focus]'
// })
// export class FocusDirective {
// 	@Input()
// 	focus : boolean;

// 	constructor(private element : ElementRef) {
// 	}

// 	protected ngOnChanges() {
//     console.log(this.element.nativeElement);
// 		if (this.focus)
// 			this.element.nativeElement.focus();
// 	}
// }

// import { Directive, Input, ElementRef, Inject } from '@angular/core';
// @Directive({
//   selector: '[focus]'
// })
// export class FocusDirective {
//   @Input()
//   focus: boolean;
//   constructor( @Inject(ElementRef) private element: ElementRef) { }
//   protected ngOnChanges() {
//     this.element.nativeElement.focus();
//     console.log(this.element);
//   }
// }

import {Directive, ElementRef, Renderer} from '@angular/core';

@Directive({selector: '[focus]'})
export class FocusDirective {
  constructor(private _elRef:ElementRef, private _renderer:Renderer) {}
  focusIf(attrValue:string) {
    //console.log(this._elRef.nativeElement.getAttribute('name'));
    if(this._elRef.nativeElement.getAttribute('name') === attrValue || this._elRef.nativeElement.getAttribute('ng-reflect-name') === attrValue) {
      this._renderer.invokeElementMethod(this._elRef.nativeElement, 'focus', []);
      this._elRef.nativeElement.select();
      return true;
    }
    return false;
  }
}