import { NgModule } from '@angular/core';
import { FocusDirective } from '../common/focus';
import { Status } from '../common/status.pipe';
import { FileName } from '../common/file.pipe';
import { VNDFormat } from '../common/vnd.pipe';
import { List } from '../common/list.pipe';
import {ClickOutsideDirective} from './click-outside.directive';

@NgModule({
  imports: [
  ],
  declarations: [FocusDirective, Status, FileName, ClickOutsideDirective, VNDFormat, List],
  exports: [FocusDirective, Status, FileName, ClickOutsideDirective, VNDFormat, List],
})
export class FocusModule { }