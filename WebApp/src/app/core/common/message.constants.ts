﻿export class MessageContstants {
  public static SYSTEM_ERROR_MSG = "Có lỗi kết nối đến máy chủ";
  public static CONFIRM_DELETE_MSG = "Bạn có chắc muốn xóa bản ghi này?";
  public static LOGIN_AGAIN_MSG = "Bạn hết phiên đăng nhập. Mời đăng nhập lại.";
  public static CREATED_OK_MSG = "Thêm mới thành công";
  public static UPDATED_OK_MSG = "Cập nhật thành công";
  public static DELETED_OK_MSG = "Xóa thành công";
  public static CONGBO_OK_MSG = "Công bố thành công";
  public static LOADD_DATA_OK_MSG = "Tải dữ liệu thành công";
  public static FORBIDDEN = "Bạn bị chặn truy cập";
  public static WARNING = "Vui lòng chọn đối tượng cần thao tác.";
  public static NOALLOW = "Bản ghi này không được phép thao tác.";
  public static XUATEXCEL_SUCCESS = "Xuất excel thành công";
}
