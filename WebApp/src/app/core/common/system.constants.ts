﻿export class SystemConstants {
  public static CURRENT_USER = "CURRENT_USER";
  public static ADMIN = "Admin";
  public static BASE_API = "https://localhost:44382";
  public static pageIndex: number = 1;
  public static pageSize: number = 20;
  public static pageDisplay: number = 5;
  public static lat: number = 21.022703;
  public static lng: number = 105.8194541;
}
