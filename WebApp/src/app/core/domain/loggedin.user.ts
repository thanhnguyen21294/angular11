﻿export class LoggedInUser {
    constructor(access_token: string, username: string, fullName: string, idAgent: string, email: string, avatar: string, roles: any, permissions: any, id: string) {
        this.access_token = access_token;
        this.fullName = fullName;
        this.username = username;
        this.email = email;
        this.avatar = avatar;
        this.roles = roles;
        this.permissions = permissions;
        this.id = id;
    }
    public id: string;
    public access_token: string;
    public username: string;
    public fullName: string;
    public idAgent: string;
    public email: string;
    public avatar: string;
    public permissions: any;
    public roles: any;
}