export enum ChiPhiDuAn {
    BoiThuongHoTroTaiDinhCu = 1,
    XayDung = 2,
    ThietBi = 3,
    QLDA = 4,
    DTXD = 5,
    Khac = 6,
    DuPhong = 7
}