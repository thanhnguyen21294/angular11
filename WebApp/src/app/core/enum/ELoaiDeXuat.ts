export enum ELoaiDeXuat {
    DonGia = 1,
    GiaVatLieu = 2,
    GiaNhanCong = 3,
    GiaCaMayVaThietBiThiCong = 4,
    ChiSoGia = 5,
    SuatVonDauTu = 6,
    QuanLyHopDongXayDung = 7
}