import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from "rxjs";
@Injectable()
export class SelectService {
  private notify = new Subject<any[]>();
  notifyObservable$ = this.notify.asObservable();

  private _coSo = new Subject<string>();
  coSo$ = this._coSo.asObservable();

  private _thongBao = new Subject<any>();
  thongBao$ = this._thongBao.asObservable();

  public notifyOther(data: any[]) {
    if (data) {
      this.notify.next(data);
    }
  }

  public CoSo(data: string){
    if(data){
      this._coSo.next(data);
    }
  }

  public ThongBao (date: any){
    if(date){
      this._thongBao.next(date);
    }
  }

}
