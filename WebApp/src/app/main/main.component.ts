import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    let body = document.getElementsByTagName('body')[0];
    body.classList.add("sidebar-mini");
    body.classList.add("wysihtml5-supported");
    body.classList.add("skin-blue-light");
    body.classList.add("fixed");
    body.classList.remove("hold-transition");
    body.classList.remove("login-page");
    // AdminLTE.init();
  }

}
