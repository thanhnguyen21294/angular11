import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainComponent } from "./main.component";
import { HeaderComponent } from "../shared/header/header.component";
import { LeftComponent } from "../shared/left/left.component";
import { FooterComponent } from "../shared/footer/footer.component";
import { UtilityService } from "../core/services/utility.service";
import { AuthenService } from "../core/services/authen.service";
import { DataService } from "../core/services/data.service";
import { NotificationService } from "../core/services/notification.service";
import { ModalModule } from "ngx-bootstrap/modal";
import { FormsModule } from "@angular/forms";
import { FocusModule } from "../core/common/forcus.module";
import { mainRoutes } from "./main.router";
import { RouterModule } from "@angular/router";
import { TabsModule } from "ngx-bootstrap/tabs";
import { BusyModule } from "angular2-busy";
import { SelectService } from '../core/services/common/select.service';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mainRoutes),
    ModalModule.forRoot(),
    FormsModule,
    FocusModule,
    TabsModule.forRoot(),
    BusyModule,
  ],
  declarations: [
    MainComponent,
    HeaderComponent,
    LeftComponent,
    FooterComponent,
  ],
  providers: [UtilityService, AuthenService, DataService, NotificationService, SelectService],
})
export class MainModule { }
