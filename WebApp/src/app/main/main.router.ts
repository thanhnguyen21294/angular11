import { Routes } from '@angular/router';
import { MainComponent } from './main.component';
// import { HomeComponent } from './home/home.component';

export const mainRoutes: Routes = [
    {
        path: '', component: MainComponent, children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            // { path: 'home', component: HomeComponent },
            { path: 'system', loadChildren: './system/system.module#SystemModule' },
            { path: 'utility', loadChildren: './utility/utility.module#UtilityModule' },
            { path: 'post', loadChildren: './post/post.module#PostModule' },
            { path: 'document', loadChildren: './document/document.module#DocumentModule' },
            { path: 'comment', loadChildren: './comment/comment.module#CommentModule' },
            { path: 'huongdan', loadChildren: './huongdan/huongdan.module#HuongdanModule' },
            { path: 'qldx', loadChildren: './quanlydexuat/quan-ly-de-xuat.module#QuanLyDeXuatModule' },
            { path: 'csdl', loadChildren: './congbo/csdl-congbo.module#CSDLCONGBOModule' },
            { path: 'giathitruong', loadChildren: './giathitruongs/giathitruongs.module#GiaThiTruongsModule' },
            { path: 'diendan', loadChildren: './diendan/diendan.module#DiendanModule' }
        ]
    }
]
