import { Component, OnInit, Input, OnChanges, EventEmitter, Output, ViewChildren } from '@angular/core';
import { FocusDirective } from '../../core/common/focus';
import { ClickOutsideDirective } from '../../core/common/click-outside.directive';

@Component({
  selector: 'dropdowntree',
  templateUrl: './dropdowntree.component.html',
  styleUrls: ['./dropdowntree.component.css']
})
export class DropdowntreeComponent implements OnInit {

  public listCheck: boolean = false;
  public text: string;
  public level: number = 0;
  public itemSelect: string;
  public listItemSearch: any[];
  @Input() ReadOnly: boolean = false;
  @Input() selectValue: string = '';
  @Input() listItem: any[];
  @Output() changeSelectValue: EventEmitter<any> = new EventEmitter();

  @ViewChildren(FocusDirective) inputs;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.selectValue == null || this.selectValue == '') {
      this.itemSelect = '';
    }
    else {
      this.itemSelect = this.selectValue;
    }
    this.setSelected();
    this.listCheck = false;
  }

  public selectItem(text: string, value: string, disable: boolean) {
    if (!disable) {
      this.text = text;
      this.listCheck = false;
      this.itemSelect = value;
      this.selectValue = null;
      if (value == "") {
        value = '';
      }
      this.changeSelectValue.emit({ value: value, text: text });
    }
  }

  myClick() {
    if (!this.ReadOnly) {
      this.listCheck = !this.listCheck;
      if (this.listCheck) {
        setTimeout(() => {
          this.FocusInput('tree-search');
        }, 50);
      }
    }
  }

  setSelected() {
    if (this.listItem && this.listItem.length > 0) {
      for (let item of this.listItem) {
        this.listItemSearch = this.listItem;
        this.timTree(item, this.itemSelect);
      }
    }
  }

  timTree(data: any, id: string) {
    if (data.id == id) {
      this.text = data.text;
      return;
    }
    if (data.children && data.children.length > 0) {
      for (let item of data.children) {
        this.timTree(item, id);
      }
    }
  }

  bodauTiengViet(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    return str;
  }

  search(event) {
    if (event) {
      let filter = event.target.value;
      filter = this.bodauTiengViet(filter)
      this.listItemSearch = this.listItem.filter(val => {
        return this.filterField(val, filter) || this.filterChildren(val.children, val, filter);
      });
    }
    // console.log(this.listItemSearch);
  }

  // isFiltered(node, filter) {
  //   if (filter) {
  //     return this.filterField(node, filter) || this.filterChildren(node.children, node, filter);
  //   } else {
  //     return true;
  //   }
  // }

  filterChildren(children: any[], parent, filter) {
    let res = false;
    if (children) {
      children.map(child => {
        // console.log(child)
        let _fields = this.filterField(child, filter);
        let _children = this.filterChildren(child.children, child, filter);
        res = _fields || _children || res;
      });
      parent.expanded = true;
    }
    return res;
  }

  // filterChildren(children: any[], parent, filter) {
  //   let res = false;
  //   if (children) {
  //     children.map(child => child.filter(
  //      val =>{ return this.filterField(val, filter) || this.filterChildren(val.children, child, filter)}
  //     ));
  //     parent.expanded = true;
  //   }
  //   return res;
  // }

  filterField(node: any, filter: String) {
    return (this.bodauTiengViet(node.text).indexOf(filter) != -1);
  }

  FocusInput(Name: string) {
    this.inputs.toArray().some(myInput =>
      myInput.focusIf(Name));
  }

  onClickedOutside(event) {
    if (this.listCheck) {
      this.listCheck = false;
    }
  }

}
