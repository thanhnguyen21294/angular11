import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FocusModule } from '../../core/common/forcus.module';
import { DropdowntreeComponent } from './dropdowntree.component';
import { TreerowComponent } from './treerow/treerow.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FocusModule
  ],
  declarations: [DropdowntreeComponent, TreerowComponent],
  exports: [DropdowntreeComponent, TreerowComponent],
})
export class DropdowntreeModule { }