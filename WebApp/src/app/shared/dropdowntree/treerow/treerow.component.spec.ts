import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreerowComponent } from './treerow.component';

describe('TreerowComponent', () => {
  let component: TreerowComponent;
  let fixture: ComponentFixture<TreerowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreerowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreerowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
