import { Component, OnInit, Input } from '@angular/core';
import { DropdowntreeComponent } from '../dropdowntree.component';

@Component({
  selector: '[treerow]',
  templateUrl: './treerow.component.html',
  styleUrls: ['./treerow.component.css']
})
export class TreerowComponent implements OnInit {

  @Input() listItem: any;
  @Input() level: number;
  @Input() itemSelect: string;

  constructor( private dropdowntree: DropdowntreeComponent) { }

  ngOnInit() {
  }

  changeValue(name, id, disable){
    this.dropdowntree.selectItem(name, id, disable);
  }

}
