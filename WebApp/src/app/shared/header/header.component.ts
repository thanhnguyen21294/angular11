import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { AuthenService } from '../../core/services/authen.service';
import { SystemConstants } from '../../core/common/system.constants';
import { UtilityService } from '../../core/services/utility.service';
import { UrlConstants } from '../../core/common/url.constants';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { MessageContstants } from '../../core/common/message.constants';
import { FocusDirective } from '../../core/common/focus';
import { Router } from '@angular/router';
import { SelectService } from '../../core/services/common/select.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @ViewChild('modalEditPass') public modalEditPass: ModalDirective;
  public user: LoggedInUser;
  public entity: any;
  public functions: any[];
  public listCoSos: any[];
  public baseFolder: string = SystemConstants.BASE_API;
  @ViewChildren(FocusDirective) inputs;

  constructor(private utilityService: UtilityService, private _authenService: AuthenService,
    private _selectService: SelectService, private _dataService: DataService, private _notificationService: NotificationService, private router: Router) {
  }

  ngOnInit() {
    this.loadMenu();
    this.user = this._authenService.getLoggedInUser();
  }

  FocusInput(Name: string) {
    this.inputs.toArray().some(myInput =>
      myInput.focusIf(Name));
  }
  private loadMenu() {
    this._dataService.get('/api/function/getlisthierarchy').subscribe((response: any[]) => {
      this.functions = this.BuildTree(response).sort((n1, n2) => {
        if (n1.DisplayOrder > n2.DisplayOrder)
          return 1;
        else if (n1.DisplayOrder < n2.DisplayOrder)
          return -1;
        return 0;
      });
      let url: any[] = this.router.url.split('/');
      let idFunction = "/" + url[1] + "/" + url[2];
      setTimeout(() => {
        let u = this.functions.filter(x => x.Url == idFunction)[0];
        if (u != undefined)
          this._selectService.notifyOther(u.children);
      }, 100);
    }, error => this._dataService.handleError(error));
  }
  activeMenu(id) {
    setTimeout(() => {
      this._selectService.notifyOther(this.functions.filter(x => x.IdFunction == id)[0].children.sort((n1, n2) => {
        if (n1.DisplayOrder > n2.DisplayOrder)
          return 1;
        else if (n1.DisplayOrder < n2.DisplayOrder)
          return -1;
        return 0;
      })
      );
    }, 100);
  }
  BuildTree = (arr: any[]): any[] => {
    let roots: any[] = [];
    roots = arr.filter(x => x.ParentId == null);
    for (var i = 0; i < roots.length; i++) {
      this.Tree(arr, roots[i]);
    }
    return roots;
  }

  Tree(arr: any[], list: any) {
    let childs = arr.filter(x => x.ParentId == list.IdFunction);
    list.children = childs;
    for (var i = 0; i < childs.length; i++) {
      this.Tree(arr, childs[i]);
    }
  }
  logout() {
    this._dataService.post('/api/appUser/logout', null).subscribe((response: any) => {
      sessionStorage.removeItem(SystemConstants.CURRENT_USER);
      localStorage.removeItem(SystemConstants.CURRENT_USER);
      this.utilityService.navigate(UrlConstants.LOGIN);
    }, error => this._dataService.handleError(error));
  }

  showEditPassModal() {
    this.entity = {};
    this.modalEditPass.show();
  }

  onShow(event) {
    this.FocusInput("currentPassword");
  }

  saveChangePass(valid: boolean) {
    if (valid) {
      this._dataService.put('/api/appUser/updatepass', JSON.stringify(this.entity))
        .subscribe((response: any) => {
          this.modalEditPass.hide();
          this._notificationService.printSuccessMessage(MessageContstants.CREATED_OK_MSG);
        }, error => this._dataService.handleError(error));
    }
  }

}
