import { Component, OnInit, OnDestroy } from "@angular/core";
import { DataService } from "./../../core/services/data.service";
import { SelectService } from '../../core/services/common/select.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-left",
  templateUrl: "./left.component.html",
  styleUrls: ["./left.component.css"],
})
export class LeftComponent implements OnInit {
  public functions: any[];
  arrowIcon: boolean = false;
  constructor(private _dataService: DataService, private _selectService: SelectService, private route: Router) { }
  ngOnInit() {
    this._selectService.notifyObservable$.subscribe((res) => {
      this.functions = res.sort((n1, n2) => {
        if (n1.DisplayOrder > n2.DisplayOrder)
          return 1;
        else if (n1.DisplayOrder < n2.DisplayOrder)
          return -1;
        return 0;
      });
      this.functions.forEach(item => {
        item.children = item.children.sort((n1, n2) => {
          if (n1.DisplayOrder > n2.DisplayOrder)
            return 1;
          else if (n1.DisplayOrder < n2.DisplayOrder)
            return -1;
          return 0;
        });
        item.active = this.isRouteActice(item.Url);
      })
    });
  }
  isRouteActice(url: string) {
    return this.route.url.indexOf(url) > -1;
  }
  private loadMenu() {
    this._dataService.get("/api/function/getlisthierarchy").subscribe(
      (response: any[]) => {
        this.functions = this.BuildTree(response).sort((n1, n2) => {
          if (n1.DisplayOrder > n2.DisplayOrder) return 1;
          else if (n1.DisplayOrder < n2.DisplayOrder) return -1;
          return 0;
        });
      },
      (error) => this._dataService.handleError(error)
    );
  }

  BuildTree = (arr: any[]): any[] => {
    let roots: any[] = [];
    roots = arr.filter((x) => x.ParentId == null);
    for (var i = 0; i < roots.length; i++) {
      this.Tree(arr, roots[i]);
    }
    return roots;
  };

  Tree(arr: any[], list: any) {
    let childs = arr.filter((x) => x.ParentId == list.IdFunction);
    list.children = childs;
    for (var i = 0; i < childs.length; i++) {
      this.Tree(arr, childs[i]);
    }
  }
}
